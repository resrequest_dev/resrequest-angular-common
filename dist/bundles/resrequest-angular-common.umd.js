(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/forms'), require('@angular/common'), require('@angular/material')) :
	typeof define === 'function' && define.amd ? define(['exports', '@angular/core', '@angular/forms', '@angular/common', '@angular/material'], factory) :
	(factory((global.resrequestAngularCommon = global.resrequestAngularCommon || {}),global.ng.core,global.ng.forms,global.ng.common,global.ng.material));
}(this, (function (exports,_angular_core,_angular_forms,_angular_common,_angular_material) { 'use strict';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/* General backward compatibility for legacy JavaScript */
if (!(/** @type {?} */ (window)).id) {
    (/** @type {?} */ (window)).id = function (objId) {
        return window.document.getElementById(objId);
    };
}
/**
 * @param {?} url
 * @param {?=} name
 * @param {?=} width
 * @param {?=} height
 * @return {?}
 */
function openPopup(url, name, width, height) {
    if (!name)
        name = "";
    if (!width)
        width = 400;
    if (!height)
        height = 300;
    var /** @type {?} */ left = screen.availWidth / 2 - width / 2;
    var /** @type {?} */ top = screen.availHeight / 2 - height / 2;
    window.open("reservation.php?" + url, name, "fullscreen=0,width=" + width + ",height=" + height + ",left=" + left + ",top=" + top);
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var AccommFilterComponent = (function () {
    function AccommFilterComponent() {
        this.selected = {
            ids: "",
            names: ""
        };
        this.disabled = false;
        this.propagateChange = function () { };
    }
    /**
     * @return {?}
     */
    AccommFilterComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (!this.name) {
            this.name = "accomm";
        }
        (/** @type {?} */ (window))["ng_accomm_filter_" + this.name] = function (ids, names) {
            _this.change(ids, names);
        };
    };
    /**
     * @param {?} value
     * @return {?}
     */
    AccommFilterComponent.prototype.writeValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        if (value) {
            this.selected.ids = value.ids;
            this.selected.names = value.names;
        }
        else {
            this.propagateChange();
        }
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    AccommFilterComponent.prototype.registerOnChange = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        var _this = this;
        this.propagateChange = function () {
            fn(_this.selected);
        };
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    AccommFilterComponent.prototype.registerOnTouched = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) { };
    /**
     * @param {?} isDisabled
     * @return {?}
     */
    AccommFilterComponent.prototype.setDisabledState = /**
     * @param {?} isDisabled
     * @return {?}
     */
    function (isDisabled) {
        this.disabled = isDisabled;
    };
    /**
     * @param {?} ids
     * @param {?} names
     * @return {?}
     */
    AccommFilterComponent.prototype.change = /**
     * @param {?} ids
     * @param {?} names
     * @return {?}
     */
    function (ids, names) {
        this.selected.ids = ids;
        this.selected.names = names;
        this.propagateChange();
    };
    /**
     * @return {?}
     */
    AccommFilterComponent.prototype.edit = /**
     * @return {?}
     */
    function () {
        if (!this.disabled) {
            openPopup("293+" + this.selected.ids + "+3+" + this.name, "property_filter_" + this.name, 556, 407);
        }
    };
    AccommFilterComponent.decorators = [
        { type: _angular_core.Component, args: [{
                    selector: 'rr-accomm-filter',
                    template: "<mat-form-field (click)=\"edit()\" class=\"filter-full filter-click\"> <input matInput placeholder=\"Property / accommodation\" readonly [(ngModel)]=\"selected.names\" id=\"{{name}}Filter\" class=\"filter-click\"> <input type=hidden [(ngModel)]=\"selected.ids\" id=\"{{name}}Ids\"> </mat-form-field> ",
                    styles: [".filter-full { width: 100%; } .filter-click { cursor: pointer } "],
                    providers: [{
                            provide: _angular_forms.NG_VALUE_ACCESSOR,
                            useExisting: AccommFilterComponent,
                            multi: true
                        }]
                },] },
    ];
    /** @nocollapse */
    AccommFilterComponent.propDecorators = {
        "name": [{ type: _angular_core.Input },],
    };
    return AccommFilterComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var ResrequestCommonModule = (function () {
    function ResrequestCommonModule() {
    }
    ResrequestCommonModule.decorators = [
        { type: _angular_core.NgModule, args: [{
                    imports: [
                        _angular_common.CommonModule,
                        _angular_forms.FormsModule,
                        _angular_material.MatFormFieldModule,
                        _angular_material.MatInputModule
                    ],
                    declarations: [
                        AccommFilterComponent
                    ],
                    exports: [
                        AccommFilterComponent
                    ]
                },] },
    ];
    return ResrequestCommonModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * Generated bundle index. Do not edit.
 */

exports.openPopup = openPopup;
exports.AccommFilterComponent = AccommFilterComponent;
exports.ResrequestCommonModule = ResrequestCommonModule;

Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=resrequest-angular-common.umd.js.map
