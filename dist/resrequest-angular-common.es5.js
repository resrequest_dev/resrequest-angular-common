import { Component, Input, NgModule } from '@angular/core';
import { FormsModule, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule, MatInputModule } from '@angular/material';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/* General backward compatibility for legacy JavaScript */
if (!(/** @type {?} */ (window)).id) {
    (/** @type {?} */ (window)).id = function (objId) {
        return window.document.getElementById(objId);
    };
}
/**
 * @param {?} url
 * @param {?=} name
 * @param {?=} width
 * @param {?=} height
 * @return {?}
 */
function openPopup(url, name, width, height) {
    if (!name)
        name = "";
    if (!width)
        width = 400;
    if (!height)
        height = 300;
    var /** @type {?} */ left = screen.availWidth / 2 - width / 2;
    var /** @type {?} */ top = screen.availHeight / 2 - height / 2;
    window.open("reservation.php?" + url, name, "fullscreen=0,width=" + width + ",height=" + height + ",left=" + left + ",top=" + top);
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var AccommFilterComponent = (function () {
    function AccommFilterComponent() {
        this.selected = {
            ids: "",
            names: ""
        };
        this.disabled = false;
        this.propagateChange = function () { };
    }
    /**
     * @return {?}
     */
    AccommFilterComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (!this.name) {
            this.name = "accomm";
        }
        (/** @type {?} */ (window))["ng_accomm_filter_" + this.name] = function (ids, names) {
            _this.change(ids, names);
        };
    };
    /**
     * @param {?} value
     * @return {?}
     */
    AccommFilterComponent.prototype.writeValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        if (value) {
            this.selected.ids = value.ids;
            this.selected.names = value.names;
        }
        else {
            this.propagateChange();
        }
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    AccommFilterComponent.prototype.registerOnChange = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        var _this = this;
        this.propagateChange = function () {
            fn(_this.selected);
        };
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    AccommFilterComponent.prototype.registerOnTouched = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) { };
    /**
     * @param {?} isDisabled
     * @return {?}
     */
    AccommFilterComponent.prototype.setDisabledState = /**
     * @param {?} isDisabled
     * @return {?}
     */
    function (isDisabled) {
        this.disabled = isDisabled;
    };
    /**
     * @param {?} ids
     * @param {?} names
     * @return {?}
     */
    AccommFilterComponent.prototype.change = /**
     * @param {?} ids
     * @param {?} names
     * @return {?}
     */
    function (ids, names) {
        this.selected.ids = ids;
        this.selected.names = names;
        this.propagateChange();
    };
    /**
     * @return {?}
     */
    AccommFilterComponent.prototype.edit = /**
     * @return {?}
     */
    function () {
        if (!this.disabled) {
            openPopup("293+" + this.selected.ids + "+3+" + this.name, "property_filter_" + this.name, 556, 407);
        }
    };
    AccommFilterComponent.decorators = [
        { type: Component, args: [{
                    selector: 'rr-accomm-filter',
                    template: "<mat-form-field (click)=\"edit()\" class=\"filter-full filter-click\"> <input matInput placeholder=\"Property / accommodation\" readonly [(ngModel)]=\"selected.names\" id=\"{{name}}Filter\" class=\"filter-click\"> <input type=hidden [(ngModel)]=\"selected.ids\" id=\"{{name}}Ids\"> </mat-form-field> ",
                    styles: [".filter-full { width: 100%; } .filter-click { cursor: pointer } "],
                    providers: [{
                            provide: NG_VALUE_ACCESSOR,
                            useExisting: AccommFilterComponent,
                            multi: true
                        }]
                },] },
    ];
    /** @nocollapse */
    AccommFilterComponent.propDecorators = {
        "name": [{ type: Input },],
    };
    return AccommFilterComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var ResrequestCommonModule = (function () {
    function ResrequestCommonModule() {
    }
    ResrequestCommonModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule,
                        FormsModule,
                        MatFormFieldModule,
                        MatInputModule
                    ],
                    declarations: [
                        AccommFilterComponent
                    ],
                    exports: [
                        AccommFilterComponent
                    ]
                },] },
    ];
    return ResrequestCommonModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * Generated bundle index. Do not edit.
 */

export { openPopup, AccommFilterComponent, ResrequestCommonModule };
//# sourceMappingURL=resrequest-angular-common.es5.js.map
