import { OnInit } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
export declare class AccommFilterComponent implements OnInit, ControlValueAccessor {
    selected: {
        ids: string;
        names: string;
    };
    private disabled;
    name: string;
    propagateChange: any;
    ngOnInit(): void;
    writeValue(value: any): void;
    registerOnChange(fn: Function): void;
    registerOnTouched(fn: () => void): void;
    setDisabledState(isDisabled: boolean): void;
    change(ids: string, names: string): void;
    edit(): void;
}
