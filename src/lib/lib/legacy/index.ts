/* General backward compatibility for legacy JavaScript */
if(!(<any>window).id) {
	(<any>window).id = function(objId: string) {
		return window.document.getElementById(objId);
	};
}

export function openPopup(url: string, name?: string, width?: number, height?: number) {
	if(!name) name = "";
	if(!width) width = 400;
	if(!height) height = 300;

	let left = screen.availWidth / 2 - width / 2;
	let top = screen.availHeight / 2 - height / 2;

	window.open(
		"reservation.php?" + url,
		name,
		"fullscreen=0,width=" + width + ",height=" + height + ",left=" + left + ",top=" + top
	);
}
