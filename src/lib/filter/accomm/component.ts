import {
	Component,
	Input,
	OnInit
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { openPopup } from '../../lib/legacy';

@Component({
	selector: 'rr-accomm-filter',
	templateUrl: './component.html',
	styleUrls: ['./component.scss'],
	providers: [{
		provide: NG_VALUE_ACCESSOR,
		useExisting: AccommFilterComponent,
		multi: true
	}]
})
export class AccommFilterComponent implements OnInit, ControlValueAccessor {
	public selected = {
		ids: "",
		names: ""
	};
	private disabled = false;

	@Input() name: string;

	propagateChange: any = () => {};

	ngOnInit() {
		if(!this.name) {
			this.name = "accomm";
		}

		(<any>window)["ng_accomm_filter_" + this.name] = (ids: string, names: string) => {
			this.change(ids, names);
		};
	}

	writeValue(value: any) {
		if(value) {
			this.selected.ids = value.ids;
			this.selected.names = value.names;
		} else {
			this.propagateChange();
		}
	}

	registerOnChange(fn: Function) {
		this.propagateChange = () => {
			fn(this.selected);
		};
	}

	registerOnTouched(fn: () => void): void { }

	setDisabledState(isDisabled: boolean) {
		this.disabled = isDisabled;
	}

	change(ids: string, names: string) {
		this.selected.ids = ids;
		this.selected.names = names;
		this.propagateChange();
	}

	edit() {
		if(!this.disabled) {
			openPopup("293+" + this.selected.ids + "+3+" + this.name, "property_filter_" + this.name, 556, 407);
		}
	}
}
