import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {
	MatFormFieldModule,
	MatInputModule
}  from '@angular/material';

import { AccommFilterComponent } from './filter/accomm/component';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		MatFormFieldModule,
		MatInputModule
	],
	declarations: [
		AccommFilterComponent
	],
	exports: [
		AccommFilterComponent
	]
})
export class ResrequestCommonModule { }
